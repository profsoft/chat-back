var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

server.listen(3000);

app.get('/', function(request, respons) {
    respons.sendFile(__dirname + '/index.html');
});

Date.prototype.getUnixTimestamp = function() {

    return Math.round(this.getTime() / 1000);

}

connections = [];
id = 1;

io.sockets.on('connection', function(socket) {
    console.log("Успешное соединение: " + socket.id);
    socket.emit("new_username", {userId: "user_" + id});
    io.sockets.emit("new_connected", {text: "Присоединился user_" + id});
    id++;
    connections.push(socket);

    socket.on('disconnect', function(data) {
        connections.splice(connections.indexOf(socket), 1);
        console.log("Отключились: " + socket.id);
    });

    socket.on('send mess', function(data) {
        var time = new Date();
        io.sockets.emit('add mess', {userId: "user_" + socket.id, text: data.text, name: data.name, time: time.getUnixTimestamp()});
    });



});